package com.socrata.datacoordinator.resources

import com.socrata.datacoordinator.service.CoordinatorErrorsAndMetrics
import com.socrata.http.server.routing.SimpleResource

trait SodaResource extends SimpleResource
